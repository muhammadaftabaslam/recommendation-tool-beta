/*
 * Stats Saving and Serving over socket. 
 * Author: Khurram Ijaz
 * copyrigths: liveadmins.com LLC
 */

var events = require('events'),
util = require('util'), url = require('url');
var statsEvent = new events.EventEmitter();
var crypto = require('crypto');
var async = require('async'); 
colors = require('colors');
var _ = require('underscore');
var fs = require('fs');
var raccoon = require('raccoon');
raccoon.connect(6379, '127.0.0.1');
exports.visitorStats_live = function(req,res){
	var visitorid = req.params.vid;		
	socketio.on('connection',visitorConnect);
	var vAct = [];
        
	function visitorConnect(socket){            
                console.log('Tracking Visitor Behavoir');
                redis.sadd('websocketsV:'+visitorid,socket.id,function(err,res){
                        // get the SK activities                
                        getVisitorData(visitorid,function(data){
                            socket.emit("visitor_updates",data);                     
                        });

                });
                
                        
		socket.on('disconnect',function(){
			redis.srem('websocketsV:'+visitorid,socket.id,function(err,rem){
				console.log('Visitor socket['+socket.id+'] removed from set');
                               // delete socketio.sockets.sockets[socket.id]; 
			});
                        console.log('Socket removed from server');
		});
		socketio.removeListener('connection', visitorConnect);		
	}	
	res.render("live_visitor",{ visitorid : visitorid });
};

exports.websiteStats_live = function(req,res){
	var websiteid = req.params.wid;		
	// security risk on accepting connection without websiteid need to confirm if websiteid exisists. TODO.
	console.log('btStats');
	socketio.on('connection',onConnect);
	
	function onConnect(socket){					
	
          redis.smembers('websockets:'+websiteid,function(err,websockets){
                if(websockets.length != 0){                                   
                    websockets.forEach(function(socketid){                   
                        if(socketio.sockets.sockets[socketid] == undefined){
                            console.log('socket id is not connected'+socketid+' deleting socket');
                            redis.srem('websockets:'+websiteid,socketid,function(err,rem){
                                    console.log('socket ['+socketid+'] removed from set');
                            });
                        }
                        else{
                            // Emit data to these sockets.
                        }
                    });
                }
                redis.sadd('websockets:'+websiteid,socket.id,function(err,rep){                        
                        console.log('new websocket listner added.');
                        getWebsiteData(websiteid,function(data){                         
                            console.log('emiting data = ',data);
                                   
                            });                       
                    });
            });
		
			
		socket.on('disconnect', function(){
			redis.srem('websockets:'+websiteid,socket.id,function(err,rem){
				console.log('socket ['+socket.id+'] removed from redis');
			});
		});	
			socketio.removeListener('connection', onConnect);		
        }
		
	console.log('bt tracking wid = ', websiteid);

	res.render("bt",{ data : "behavoir tracking"});
}

/* 
 * 
 * @param {type} visitorid
 * @param {type} callback
 * @returns {undefined}
 */
 function tempFuntion(visitorid){
	console.log('infunction',visitorid);
	var multi = redis.multi();
	var activities1 = [];
	var activities12 = [];
	multi.zrange('ttid:'+visitorid,0,-1);
	multi.exec(function(errors, results) {
	activities1 = results[0];
	async.each(activities1,function(id,callback){
                           redis.hgetall(id,function(err,act){
                               activities12.push(act);                                
                               callback();
                           });                        
                   },function(err){
                    console.log('activities12',activities12);       
                     });       
	
	});
	
	
 }
function getVisitorData(visitorid,callback){
    var Visitor = {};
    var websiteid = '';
    var activities = [];
	var multi = redis.multi();
	var activities1 = [];
	var activities12 = [];
	multi.zrange('ttid:'+visitorid,0,-1);
	multi.exec(function(errors, results) {
	activities1 = results[0];
	async.each(activities1,function(id,callback){
                           redis.hgetall(id,function(err,act){
                               activities12.push(act);                                
                               callback();
                           });                        
                   },function(err){
                    Visitor.visitor_times = activities12;       
                     });       
	
	});
	
    // get visitor website id and Behavoir activity range
     redis.get(visitorid,function(err,webid){
         Visitor.websiteid = webid;
         websiteid = webid;
         // get visitor posts
        redis.lrange(websiteid+':'+visitorid+':posts',0,-1,function(err,range){
            Visitor.bt_range = range;    
        // get visitor activities range
               redis.zrange('activities:'+visitorid,0,-1,'WITHSCORES',function(err,ids){                                                
                   // using async module to finish the .each loop first and then process other actitivies. 
                   // could also be changed to run parallel but for now it is sequentical.
                   async.each(ids,function(id,callback){
                           redis.hgetall(id,function(err,act){
                               activities.push(act);                                
                               callback();
                           });                        
                   },function(err){
                    Visitor.search_activities =  activities;
                         // get keywords against flag 0    
                             redis.hgetall('vKeyword:'+visitorid,function(err,v_keywords){
                                 Visitor.search_keywords = v_keywords;
                                 // get number of hits of this visitor
                                 redis.hget('vcount:'+websiteid,visitorid,function(err,hits){
                                     Visitor.hit_count = hits;
                                     callback(Visitor);
                                 });
                             });
                     });                    
                });   

            });// post range
        });// get website id against a visitor id
}

function getWebsiteData(websiteid,callback){
	//console.log('Enter in website func',websiteid);
	var multi = redis.multi();
    var data = {};
	var filters = getBehavoirTrackingFilters();
    var _top_filters = {};
    multi.hgetall('maxPid:'+websiteid);
    multi.hget('total_hits',websiteid);
    multi.smembers('website_id:'+websiteid);
    multi.hgetall('wKeyword:'+websiteid+':flag:0');	
    multi.hgetall(websiteid+":filters:");
	async.each(filters,function(filter,callback){                                   
		redis.hgetall(websiteid+":"+filter,function(err,filter_data){
			_top_filters[filter] = filter_data;
		callback();
		});
		},function(err){                                    
		data.top_filters = _top_filters;                                    
		});
	multi.exec(function(errors, results) {
		
	data.pids = {};
	data.pids = results[0];
	data.hits = results[1];
	data.visitors = results[2];
	data.keywords = results[3];
	data.bt_filters = results[4];
	callback(data);
	
	});
}

function pushUpdatesToLiveAdmin(websiteid,visitorid){    
        redis.smembers('websocketsV:'+visitorid,function(err,visitor_sockets){
            if(visitor_sockets.length != 0){
                getVisitorData(visitorid,function(data){                    
                    visitor_sockets.forEach(function(socketid){
                        socketio.sockets.sockets[socketid].emit("visitor_updates",data);    
                    });
                    
                });
            }
            else {
                console.log('No Live Socket connection for visitor'.blue);
            }
        });        
	redis.smembers('websockets:'+websiteid, function(err, web_sockets){					
		if(web_sockets.length != 0){ 
			getWebsiteData(websiteid,function(data){
                            web_sockets.forEach(function(socketid){				
                                socketio.sockets.sockets[socketid].emit('updates',data);
                             });// foreach
                        });
		}// if
                else{
                    console.log("No Live Socket connection for website".blue);
                }
	});// websocket
}
	
        
function push(data){
    var websiteid = data.websiteid;
    var visitorid = data.visitorid;
    redis.smembers('websocketsV:'+visitorid,function(err,visitor_sockets){
            if(visitor_sockets.length != 0){                                   
                visitor_sockets.forEach(function(socketid){
                    
                    if(socketio.sockets.sockets[socketid] == undefined){
                        console.log('socket id is not connected'+socketid+' deleting socket');
                        redis.srem('websocketsV:'+visitorid,socketid,function(err,rem){
				console.log('socket removed from set');
			});
                    }
                    else{
                        getVisitorData(visitorid,function(data){
                        socketio.sockets.sockets[socketid].emit("visitor_updates",data);            
                        });
                        
                    }
                    
                    
                });
            }
            else {
                console.log('No Live Socket connection for visitor'.blue);
            }
        });
        redis.smembers('websockets:'+websiteid,function(err,websockets){
            if(websockets.length != 0){                                   
                websockets.forEach(function(socketid){                   
                    if(socketio.sockets.sockets[socketid] == undefined){
                        console.log('socket id is not connected'+socketid+' deleting socket');
                        redis.srem('websockets:'+websiteid,socketid,function(err,rem){
				console.log('socket removed from set');
			});
                    }
                    else{
                        console.log('socket id is connected : '+socketid);
                        getWebsiteData(websiteid,function(data){
                            data.keywords = {};
                           socketio.sockets.sockets[socketid].emit("updates",data);     
                        });
                        
                        
                        
                        
                        
                    }
                    
                });
            }
            else {
                console.log('No Live Socket connection for website'.blue);
            }
        });
    
}

function ajaxPost(req,res){
     console.log("post received"+req.body.data.green);
	var postType = JSON.parse(req.body.data).postType;	

    //some change;
    var data = JSON.parse(req.body.data);
        
	// Setting website id against visitor id; so that you  can get the website the visitor went to. 
	redis.set(data.visitorid,data.websiteid);
	/* 
	// Security risk on accepting open ajax request without unique_key
	// So basically key will be generated for each new website and should be uploaded on that website and recevied back through
	   ajax post confirming the post comming from authenticated script.
            Global Counters		
		Unique Visitor id tracking per websites
			DataStructure SET
			Key	website_id
			Value : visitorid
	*/
       // Recording unique visitor ids per website. 
	redis.sadd('website_id:'+data.websiteid,data.visitorid);
	// Total hits on a website
	redis.hincrby('total_hits',data.websiteid,1);
	/*
		Result Flag 0, 1, 2 tracking globally on a website.
	*/
	redis.hincrby('resultFlag:'+data.websiteid,data.flag,1);	
	// Max PID searched on the whole website
	redis.hincrby('maxPid:'+data.websiteid, data.pid, 1);	
	// Keywords	
	
	
	/*
		Two types of posts
		1. Searched Keywords 
		2. Behavoir Tracking
	*/
	
	switch(postType){
            case 'BT':BehavoirTracking(req,res); break;	
            case 'SK':SearchKeywordTracking(req,res); break;
			case 'TM':TimeTracking(req,res); break;
            //case 'AU':ActionUrlTracking(req,res); break;
            default:
            res.end();
	}
}

exports.searchPost = function(req,res){
    
    var queryString = url.parse(req.url,true).query;
    if(_.isEmpty(queryString)){
        ajaxPost(req,res);
    }
    else{
        console.log('GET Request with query string = ',queryString);
    }
    
   
}

/*
 * Behavoir Type Post Tracking. 
 * Only selected filters are recorded and counted uniquely 
 */
function BehavoirTracking(req,res){
	
	var data = JSON.parse(req.body.data);
	var post_id = 0;
	data['datetime'] = new Date();
	
	redis.hincrby('vcount:'+data.websiteid,data.visitorid,1,function(err,count){
				RecordingBehavoir(count);
	});
	function RecordingBehavoir(){
		_storeFilterSelectedData();
		_processPostData();
		//pushUpdatesToLiveAdmin(data.websiteid,data.visitorid);
                push(data);
	}
	
	function _processPostData(){		
		redis.hincrby(data.websiteid+':filters:',JSON.stringify(_getFiltersInUse()),1);		
		redis.lpush(data.websiteid+':'+data.visitorid+':posts',  JSON.stringify(data));		
	}
	function _storeFilterSelectedData(){
		var filters = _getFiltersInUse();
		//var len = Object.keys(filters).length;
		Object.keys(filters).forEach(function(filter){			
				//console.log('data['+filter+'] = '+data[filter]);
				redis.hincrby(data.websiteid+':'+filter,data[filter],1);
					
		});
		
	}
	function _getFilterUniqueHash(){
		var md5 = crypto.createHash('md5');
		md5 = md5.update(JSON.stringify(_getFiltersInUse())).digest('hex');		
		return md5;
		
	}
        
	function _getFiltersInUse(){
		var WEBSITE_FILTERS = {tp:'',mk:'',md:'',yr:'',pr:'',co:'',fe:'',bd:'',en:'',tn:'',ml:'',dt:'',fl:'',st:'',actionurl:'',pid:''};
		Object.keys(data).forEach(function(postKey){
                    if(postKey in WEBSITE_FILTERS){	
                        if(data[postKey] !== ''){
                            WEBSITE_FILTERS[postKey] = '1';					
                        }
                        else{
                            delete WEBSITE_FILTERS[postKey];
                        }
                    }
		});
		return WEBSITE_FILTERS;
	}
	
	res.end();
	
}

function getBehavoirTrackingFilters(){
    return new Array("tp","mk","md","yr","pr","co","fe","bd","en","tn","ml","dt","fl","st","actionurl","pid");
}

function SearchKeywordTracking(req,res){
	var data = JSON.parse(req.body.data);	
	console.log('Tracking searched keywords');
	var activity_id = 0;
	
        // Global Tracking
	redis.hincrby('wKeyword:'+data.websiteid+':flag:'+data.flag,data.searchedtext,1);	
	
	redis.hincrby('vcount:'+data.websiteid,data.visitorid,1,function(err,count){		
        activity_id = count;
        redis.hincrby('vKeyword:'+data.visitorid,data.searchedtext,1);
        // Assumption visitor id can never be same on different websites.
        redis.zadd('activities:'+data.visitorid, new Date().getTime(), 'aid:'+data.websiteid+':'+data.visitorid+':'+activity_id );
        redis.hmset('aid:'+data.websiteid+':'+data.visitorid+':'+activity_id, 
                    "websiteid",  data.websiteid,
                     "visitorid",  data.visitorid,
                     "searchedtext",  data.searchedtext,
                     "flag",  data.flag,
                     "actionurl", data.actionurl,
                     "websiteurl",  data.websiteurl,
                     "pid",  data.pid,
                     "datetime", new Date()
                     );
                     push(data);
                     //pushUpdatesToLiveAdmin(data.websiteid,data.visitorid);
                    res.end();				
            });
	
}
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
function TimeTracking(req,res){
	//fs.unlinkSync('./files/New_file');
	var data = JSON.parse(req.body.data);	
	//fs.appendFile("./files/"+data.websiteid,req.body.data, function(err) {
	//	if(err) {
	//		console.log(err);
	//	} else {
	//		console.log("The file was saved!");
	//	}
	//}); 
	
	//fs.readFile("./files/2174", "utf8", function(error, data) {
	//  console.log(data);
	//});
	console.log('Time Tracking');
	var activity_id = 0;
	var d = new Date();
	var n = d.getDate();
	var n = 0;
	//raccoon.recommendFor(data.visitorid, 0, function(results){
	//console.log("Results = "+results);
	//callback(results);
	  // returns an ranked sorted array of itemIds which represent the top recommendations
	  // for that individual user based on knn.
	  // numberOfRecs is the number of recommendations you want to receive.
	  // asking for recommendations queries the 'recommendedSet' sorted set for the user.
	  // the movies in this set were calculated in advance when the user last rated
	  // something.
	  // ex. results = ['batmanId', 'supermanId', 'chipmunksId']
	//});
	//raccoon.liked(data.visitorid, data.actionurl);
	redis.hgetall('PageCount:'+data.visitorid+':'+data.actionurl,function(err,page_count){
		//console.log('Page count before',page_count[data.actionurl]);
		//console.log('type :',typeof page_count);
		
		if (isEmpty(page_count))
		{
			redis.hincrby('PageCount:'+data.visitorid+':'+data.actionurl,data.actionurl,1,function(err,count){raccoon.liked(data.visitorid, data.actionurl, function(){
			
			console.log("Error : ");
			
			});		
			activity_id = count;
			//console.log('is empty', count);
			redis.zadd('ttid:'+data.visitorid, new Date().getTime(),'tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+activity_id );
			redis.hmset('tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+activity_id, 
                    "websiteid", data.websiteid,
                     "visitorid",  data.visitorid,
                     "actionurl", data.actionurl,
                     "websiteurl",  data.websiteurl,
					 "time",  data.time,
					 "active",  data.active,
                     "datetime", d
                     );			
            });
		}
		else
		{
			//console.log('is not empty');
			redis.hgetall('PageCount:'+data.visitorid+':'+data.actionurl,function(err,check){
			n = check[data.actionurl];
			redis.hgetall('tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+n,function(err,dataaa){
				//console.log('dataaa :',dataaa);	
				//console.log('type :',typeof dataaa.active);
				//console.log('n: ',n);    
				//console.log(typeof dataaa["active"]);
				if(dataaa["active"] === "true")
				{
					//console.log('true');
					redis.hmset('tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+n, 
                    "websiteid", data.websiteid,
                     "visitorid",  data.visitorid,
                     "actionurl", data.actionurl,
                     "websiteurl",  data.websiteurl,
					 "time",  data.time,
					 "active",  data.active,
                     "datetime", d
                     );
				}
				else
				{
					//console.log('false');
					redis.hincrby('PageCount:'+data.visitorid+':'+data.actionurl,data.actionurl,1,function(err,count){
					//console.log('count : ',count);
					activity_id = count;
					redis.zadd('ttid:'+data.visitorid, new Date().getTime(),'tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+activity_id );
					redis.hmset('tid:'+data.websiteid+':'+data.visitorid+':'+data.actionurl+':'+activity_id, 
                    "websiteid", data.websiteid,
                     "visitorid",  data.visitorid,
                     "actionurl", data.actionurl,
                     "websiteurl",  data.websiteurl,
					 "time",  data.time,
					 "active",  data.active,
                     "datetime", d
                     );  			
						});
					
				}
			
                                 
                    });
			});
		
		}
		
		
		});
		push(data);
		res.end();		
	
}
