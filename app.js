
/**
 * Behavoir Tracking
 * Liveadmins
 * copyright 2014.
 * Author: Khurram.Ijaz 
 * 
 * Technologies Used
 * 
 * Node Server
 * Socket Server
 * Redis Server 
 * 
 */
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
var express = require('express');
var stats = require('./routes/stats');
var http = require('http');
var path = require('path');
var fs = require('fs');
var raccoon = require('raccoon');
//var fileName = "foo.txt";
// shows colors on console. just here for testing. 
colors = require('colors');
socketio = require('socket.io');

var redis_store = require('redis');
var async = require('async');
redis = redis_store.createClient();

redis.on('error', function(err){
	console.log(("REDIS ERROR: " + redis.host + " : " + redis.port + " - " + err).red );
});

var app = express();
app.use(allowCrossDomain);
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.cookieParser());
app.use(express.session({cookie: { path: '/', httpOnly: true, maxAge: null }, secret:'liveadmins'}));
app.use(express.methodOverride());
//app.use(express.bodyParser());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.post('/ajaxpost',stats.searchPost);
app.get('/ajaxpost',stats.searchPost);
app.get('/bt/visitor/:vid', stats.visitorStats_live);
app.get('/bt/website/:wid', stats.websiteStats_live);
app.get('/',function(req,res){
	res.send("This page may be used for checking Dubai server stats i guess");
});


//var server = app.listen(app.get('port'));
var server = http.createServer(app);
socketio = socketio.listen(server,{
    //'heartbeats' : false,    
    'browser client etag' : true,
    'log level' : 2,
    
});


server.listen(app.get('port'), function(){
  console.log(('Express server listening on port ' + app.get('port')).green);  
});
// Please research how we can detach the socket server fron express node app
/*
 1. Research on Middle ware (senja touch connect )
 2. socket server in separate file
 3. Remove the socket code from stats.js
 4. Make helper functions like below
    a. getVisitor(visitor id)
      i. get live activities 
     ii. check ( if file exsist for this visitor  = true){
		read the previous stats from file and send it . 
	} 
	else{
		send the live stats only. 
		wait for the end of chat
		record the session into the file 
		file name will be visitor.id
	}
 5. Research on GRUNT, 
 6. Unit testing framework for node.js
 7. as discussed we do not neeed http sever for .NET rather only TCP server wouold be enough please research create a new repository and 
connect the .net app with socket server. 
8. Please research and use SOCKET.IO library to save teh reference of the connected client instead of using redis to store. 
9. Make the Object Orieented Code and throw exceptions in case socket gets closed. if it does how to re-establish the same closed socket again. 
10. Kindly document the project and before implementation get approval of the tasks. 
11. 
      
*/
socketio.sockets.on('connection', function (socket) {
  
  console.log("connected yet");
  socket.on('VisitorID', function (data) {
	var visitorid = data;
    console.log("Visitor ID : ",data);
	var Visitor = {};
	var multi = redis.multi();
	var activities1 = [];
	var activities12 = [];
	multi.zrange('ttid:'+visitorid,0,-1);
	multi.exec(function(errors, results) {
	activities1 = results[0];
	console.log("B4");
	async.each(activities1,function(id,callback){
                           redis.hgetall(id,function(err,act){
                               activities12.push(act);                                
                               callback();
                           });                        
                   },function(err){
				    console.log("After");
                    Visitor.visitor_times = activities12;
					console.log("activities12 ID : ",Visitor);
					socket.emit('news',Visitor);
                    });  
		
		
	});
	
	console.log("Visitor ID : ",Visitor);
	
	
	
	
	
  });
  
});
